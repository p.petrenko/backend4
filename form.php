<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>WEB-Project</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.7">
    <link rel="preconnect" href="https://fonts.gstatic.com/">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link href="style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
  </head>
  <body>
    <header id="header_site">
      <div id="logo_and_name">
        <img id="logo" src="https://www.flaticon.com/svg/static/icons/svg/203/203678.svg" alt="Logo" />
        <h1>IndexPro</h1>
      </div>
    </header>
    <div class="container py-5 my-3 px-4" id="main">
      <div class="row d-flex">
        <div class="col-12 order-3 my-3 order-md-2">
          <table class="my-0 mx-auto" id="special">
            <tr>
              <th class="py-1 py-md-2 px-md-4" colspan="4">Курсы обучения веб-разработке</th>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">Время/Дни занятий</td>
              <td class="py-1 py-md-2 px-md-4">Понедельник</td>
              <td class="py-1 py-md-2 px-md-4">Среда</td>
              <td class="py-1 py-md-2 px-md-4">Пятница</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">13:00</td>
              <td class="py-1 py-md-2 px-md-4">Основы баз данных</td>
              <td class="py-1 py-md-2 px-md-4">PHP</td>
              <td class="py-1 py-md-2 px-md-4">Javascript</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">14:00</td>
              <td class="py-1 py-md-2 px-md-4">Методологии разработки</td>
              <td class="py-1 py-md-2 px-md-4">Основы баз данных</td>
              <td class="py-1 py-md-2 px-md-4">PHP</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">15:00</td>
              <td class="py-1 py-md-2 px-md-4" colspan="3">HTML/CSS</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">16:00</td>
              <td class="py-1 py-md-2 px-md-4">Javascript</td>
              <td class="py-1 py-md-2 px-md-4">Методологии разработки</td>
              <td class="py-1 py-md-2 px-md-4">Основы баз данных</td>
            </tr>
            <tr>
              <td class="py-1 py-md-2 px-md-4">17:00</td>
              <td class="py-1 py-md-2 px-md-4">PHP</td>
              <td class="py-1 py-md-2 px-md-4">Javascript</td>
              <td class="py-1 py-md-2 px-md-4">Методологии разработки</td>
            </tr>
          </table>
        </div>
        <div class="col-12 my-3 order-2 order-md-3" id="whole_form">
          <h6>Форма</h6>
          <form action="."
            method="POST">
            <label id="label_for_name">
            Имя:<br>
            <input name="name"
              placeholder="Введите имя"
              value='<?php print "{$name_field_value}" ?>'/>
            </label><br><br>
            <label id="label_for_email">
            E-mail:<br>
            <input name="email"
              placeholder="Введите email"
              value='<?php print "{$email_field_value}" ?>'
              type="email" />
            </label><br><br>
            <label id="label_for_birthday">
            Дата рождения:<br />
            <input name="birthday" id="birth_date"
              value='<?php print "{$bd_field_value}" ?>'
              type="date" />
            </label><br><br>
            Пол:<br>
            <label><input type="radio" <?php if(is_null($_COOKIE['gender_last_value']) or !empty($_COOKIE['gender_error'])) print "checked" ?> name="gender" value="M" 
              <?php if($_COOKIE['gender_last_value'] == 'M') print "checked" ?> />
            Мужской
            </label>
            <label><input type="radio" name="gender" value="F" 
              <?php if($_COOKIE['gender_last_value'] == 'F') print "checked" ?> />
            Женский
            </label>
            <label><input type="radio" name="gender" value="NB" 
              <?php if($_COOKIE['gender_last_value'] == 'NB') print "checked" ?> />
            Небинарная персона
            </label><br><br>
            Количество конечностей:<br>
            <label><input type="radio" name="limb_number" value="0" 
              <?php if($_COOKIE['limb_number_last_value'] == 0) print "checked" ?> />
            0
            </label>
            <label><input type="radio" name="limb_number" value="1" 
              <?php if($_COOKIE['limb_number_last_value'] == 1) print "checked" ?> />
            1
            </label>
            <label><input type="radio" name="limb_number" value="2" 
              <?php if($_COOKIE['limb_number_last_value'] == 2) print "checked" ?> />
            2
            </label>
            <label><input type="radio" name="limb_number" value="3" 
              <?php if($_COOKIE['limb_number_last_value'] == 3) print "checked" ?> />
            3
            </label>
            <label><input type="radio" name="limb_number" value="4" 
              <?php if($_COOKIE['limb_number_last_value'] == 4) print "checked" ?>/>
            4
            </label>
            <label><input type="radio" <?php if(is_null($_COOKIE['limb_number_last_value']) or !empty($_COOKIE['limb_number_error'])) print "checked" ?> name="limb_number" value="5"  
              <?php if($_COOKIE['limb_number_last_value'] == 5) print "checked" ?> />
            Другое
            </label>
            <br><br>
            <label>
              Сверхспособности:
              <br>
              <select name="superpowers[]" multiple="multiple">
                <option value="1" <?php if(!empty($_COOKIE['sp1_last_value'] && $_COOKIE['sp1_last_value'])) print "selected" ?>>Бессмертие</option>
                <option value="2" <?php if(!empty($_COOKIE['sp2_last_value'] && $_COOKIE['sp2_last_value'])) print "selected" ?>>Прохождение сквозь стены</option>
                <option value="3" <?php if(!empty($_COOKIE['sp3_last_value'] && $_COOKIE['sp3_last_value'])) print "selected" ?>>Левитация</option>
              </select>
            </label>
            <br><br>
            <label>
            Биография:<br>
            <textarea class="message" id="biography" placeholder="Расскажите о себе" rows="7" cols="50" name="biography"><?php print "{$biography_field_value}" ?></textarea>
            </label><br><br>
            <br>
            <label for="cb" class="c_box" id="label_for_cb">
            <input type="checkbox" id="cb" value="" name="contract"><span></span>
            С контрактом ознакомлен
            </label><br>
            <a id="submit_button"></a>
            <input class="py-2 px-3 mt-0 mb-4" type="submit" value="Отправить"><br>
          </form>
        </div>
      </div>
    </div>
    <a id="before_footer"></a>
    <footer id="footer_site">
      <b>@In code we trust</b>
    </footer>
  </body>
</html>
