<?php
header('Content-Type: text/html; charset=UTF-8');

//Only report fatal errors and parse errors.
error_reporting(E_ERROR | E_PARSE);

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  //include('form.php');
  $response_message = '';
  $errors_fields = '';

  $name_field_value = $_COOKIE['last_success_name'];
  $email_field_value =  $_COOKIE['last_success_email'];
  $bd_field_value =  $_COOKIE['last_success_bd'];
  $biography_field_value =  $_COOKIE['last_success_biography'];

  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    $response_message .= 'Спасибо, результаты сохранены.';
  }
  else {
    $errors = FALSE;
    $response_message .= "Отправка данных прервана из-за следующих ошибок: <br/>";
    $biography_field_value =  $_COOKIE['biography_last_value'];

    $name_field_value = $_COOKIE['name_last_value'];;
    if(!empty($_COOKIE['name_error'])) {
      $response_message .= 'Вы не заполнили имя.<br/>';
      $errors_fields .= '#label_for_name';
      $errors = TRUE;
    }

    $email_field_value = $_COOKIE['email_last_value'];
    if(!empty($_COOKIE['email_error'])) {
      $response_message .= 'Вы неверно заполнили email или не ввели данные.<br/>';
      if(!empty($errors_fields))
        $errors_fields .= ', ';
      $errors_fields .= '#label_for_email';
      $errors = TRUE;
    }

    $bd_field_value = $_COOKIE['birthday_last_value'];
    if(!empty($_COOKIE['birthday_error'])) {
      $response_message .= 'Вы неверно заполнили дату рождения или не ввели данные.<br/>';
      if(!empty($errors_fields))
        $errors_fields .= ', ';
      $errors_fields .= '#label_for_birthday';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['gender_error'])) {
      $response_message .= 'Вы неверно заполнили пол.<br/>';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['limb_number_error'])) {
      $response_message .= 'Вы ввели недопустимое количество конечностей.<br/>';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['superpowers_error'])) {
      $response_message .= 'Вы ввели неверные суперспособности.<br/>';
      $errors = TRUE;
    }
    if(!empty($_COOKIE['contract_error'])) {
        $response_message .= 'Вы не ознакомились с контрактом.<br/>';
        if(!empty($errors_fields))
          $errors_fields .= ', ';
        $errors_fields .= '#label_for_cb';
        $errors = TRUE;
    }
    if(!$errors) {
      $response_message = '';
      $name_field_value = $_COOKIE['last_success_name'];
      $email_field_value =  $_COOKIE['last_success_email'];
      $bd_field_value =  $_COOKIE['last_success_bd'];
      $biography_field_value =  $_COOKIE['last_success_biography'];
    }
  }
  include('form.php');
  if(!empty($response_message)) {
    print "<link href='show_mb.css' rel='stylesheet'>
            <div id='modal_blackout'>
              <div id='success_form_response'>
                  {$response_message}
              </div>
            </div>";
    if(!empty($errors_fields)) {
        print "<style>
          {$errors_fields} {
            text-decoration: underline 1px wavy rgba(193,56,0,1);
            -webkit-text-decoration-line: underline;
            -webkit-text-decoration-style: wavy;
            -webkit-text-decoration-color: rgba(193,56,0,1);
            padding-bottom: 1px;
            text-underline-position: under;
          }
          </style>";      
    }
  }
    exit();
}

$trimmedPost = [];

$valid_errors = FALSE;

foreach ($_POST as $key => $value)
	if (is_string($value))
		$trimmedPost[$key] = trim($value);
	else
		$trimmedPost[$key] = $value;

setcookie('name_last_value', $trimmedPost['name'] , 0);
if (empty($trimmedPost['name'])) {
  //print('Заполните имя.<br/>');
  setcookie('name_error', 1 , 0);
  $valid_errors = TRUE;
}
else setcookie('name_error', '', 1);

setcookie('email_last_value', $trimmedPost['email'], 0);
if (!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $trimmedPost['email'])) {
  //print('Заполните email.<br/>');
  setcookie('email_error', 1, 0);
  $valid_errors = TRUE;
}
else setcookie('email_error', '', 1);

setcookie('birthday_last_value', $trimmedPost['birthday'], 0);
if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $trimmedPost['birthday'])) {
  //print('Заполните дату рождения.<br/>');
  setcookie('birthday_error', 1, 0);
  $valid_errors = TRUE;
}
else setcookie('birthday_error', '', 1);

if (!preg_match('/^[MF]$|^(NB)$/', $trimmedPost['gender'])) {
  //print('Заполните пол.<br/>');
  setcookie('gender_error', '1', 0);
  $valid_errors = TRUE;
}

else setcookie('gender_error', '', 1);
setcookie('gender_last_value', $trimmedPost['gender'], 0);

if (!preg_match('/^[0-5]$/', $trimmedPost['limb_number'])) {
  //print('Заполните количество конечностей.<br/>');
  setcookie('limb_number_error', '1', 0);
  $valid_errors = TRUE;
}

else setcookie('limb_number_error', '', 1);
setcookie('limb_number_last_value', $trimmedPost['limb_number'], 0);

setcookie('biography_last_value', $trimmedPost['biography'], 0);

setcookie('superpowers_error', '', 1);

setcookie('sp1_last_value', false, 0);
setcookie('sp2_last_value', false, 0);
setcookie('sp3_last_value', false, 0);

foreach ($trimmedPost['superpowers'] as $v) {
  if (!preg_match('/[1-3]/', $v)) {
    //print('Неверные суперспособности.<br/>');
    setcookie('superpowers_error', '1', 0);
    $valid_errors = TRUE;
  }
  else {
    $current_sp = 'sp' . $v . '_last_value';
    setcookie($current_sp, true, 0);
  }
}

if (!isset($trimmedPost['contract'])) {
  //print('Вы не ознакомились с контрактом.<br/>');
  setcookie('contract_error', '1', 0);
  $valid_errors = TRUE;
}

else setcookie('contract_error', '', 1);

/*foreach ($trimmedPost as $key => $value)
  echo $key,"=>",$value, '<br />';

foreach ($trimmedPost['superpowers'] as $v)
  echo $v,'<br />';*/

if ($valid_errors) {
  setcookie('save', '', 100000);
  header('Location: index.php');
  exit();
}

$user = 'u20323';
$pass = '6300522';
$db = new PDO('mysql:host=localhost;dbname=u20323', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

try {
  $db->beginTransaction();
  $stmt1 = $db->prepare("INSERT INTO Form SET name = ?, email = ?, birthday = ?, 
    gender = ? , limb_number = ?, biography = ?");
  $stmt1 -> execute([$trimmedPost['name'], $trimmedPost['email'], $trimmedPost['birthday'], 
    $trimmedPost['gender'], $trimmedPost['limb_number'], $trimmedPost['biography']]);
  $stmt2 = $db->prepare("INSERT INTO Form_Abilities SET form_id = ?, ability_id = ?");
  $id = $db->lastInsertId();
  foreach ($trimmedPost['superpowers'] as $s)
    $stmt2 -> execute([$id, $s]);
  $db->commit();
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  $db->rollBack();
  exit();
}

setcookie('save', '1');

setcookie('last_success_name', $trimmedPost['name'], time()+3600 * 24 * 365);
setcookie('last_success_email', $trimmedPost['email'], time()+3600 * 24 * 365);
setcookie('last_success_bd', $trimmedPost['birthday'], time()+3600 * 24 * 365);
setcookie('last_success_biography', $trimmedPost['biography'], time()+3600 * 24 * 365);

setcookie('name_last_value', '', 1);
setcookie('email_last_value', '', 1);
setcookie('birthday_last_value', '', 1);
setcookie('biography_last_value', '', 1);

header('Location: index.php');
